def resultado = new StringBuilder() //<1>
def error     = new StringBuilder()

def comando = "ls -lt".execute() //<2>
comando.consumeProcessOutput(resultado, error) //<3>
comando.waitForOrKill(1000) //<4>

if (!error.toString().equals("")) //<5>
    println "Error al ejecutar el comando"
else{
    println "Ejecutado correctamente"
    println resultado //<6>

}
