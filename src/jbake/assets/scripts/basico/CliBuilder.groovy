def cli = new CliBuilder(usage: 'groovy CliBuilder.groovy -[habcd]')

cli.with { // <1>
     h(longOpt: 'help',    'Usage Information \n', required: false)
     a(longOpt: 'Option a','Al seleccionar "a" pinta seleccionada -> a ', required: false)
     b(longOpt: 'Option b','Al seleccionar "b" pinta seleccionada -> b ', required: false)
     c(longOpt: 'Option c','Al seleccionar "c" pinta seleccionada -> c ', required: false)
     d(longOpt: 'Option d','Al seleccionar "d" pinta seleccionada -> d ', required: false)
}

def options = cli.parse(args)

if (!options) {
    return
}
if (options.h) { // <2>
    cli.usage()
    return
}
if (options.a) { // <3>
    println "------------------------------------------------------------------"
    println "Seleccionada ha sido la 'a'"
    println "------------------------------------------------------------------"
}
if (options.b) {
    println "------------------------------------------------------------------"
    println "Seleccionada ha sido la 'b'"
    println "------------------------------------------------------------------"
}
if (options.c) {
    println "------------------------------------------------------------------"
    println "Seleccionada ha sido la 'c'"
    println "------------------------------------------------------------------"
}
if (options.d) {
    println "------------------------------------------------------------------"
    println "Seleccionada ha sido la 'd'"
    println "------------------------------------------------------------------"
}