//tag::dependencies[]
@GrabConfig(systemClassLoader=true)
@Grab(group='org.groovyfx',module='groovyfx',version='8.0.0',transitive=false, noExceptions=true)
//end::dependencies[]

import groovyx.javafx.beans.FXBindable
import static groovyx.javafx.GroovyFX.start

//tag::business[]
class Business {
    String server

    @FXBindable
    String delay = ''   //<1>


    void refresh() {
        Date start = new Date()
        if (!isReachable(10 * 1000)) { //10seg max
            delay = "DOWN"
            return
        }
        Date stop = new Date()
        groovy.time.TimeDuration td = groovy.time.TimeCategory.minus(stop, start)
        delay = "$td"   //<2>
    }

    boolean isReachable(int timeOutMillis, int port = 80) {
        try {
            Socket soc = new Socket()
            soc.connect(new InetSocketAddress(server, port), timeOutMillis);
            true
        } catch (IOException ex) {
            println ex.toString()
            false;
        }
    }
}
//end::business[]

start {
    def business = new Business(server: args[0])

    //tag::vista[]
    stage(title: 'Latencia', visible: true) {                               //<1>
        scene(fill: BLACK, width: 800, height: 250) {
            hbox(padding: 60) {
                text(text: "Ping to $business.server  :", font: '20pt sanserif') {    //<2>
                    fill linearGradient(endX: 0, stops: [PALEGREEN, SEAGREEN])
                }
                label(text: bind(business, 'delay'), font: '40pt sanserif') {    //<3>
                    fill linearGradient(endX: 0, stops: [CYAN, DODGERBLUE])
                    effect dropShadow(color: DODGERBLUE, radius: 25, spread: 0.25)
                }
            }
        }
    }
    //end::vista[]

    //tag::bucle[]
    sequentialTransition(cycleCount: INDEFINITE) {  //<1>
        pauseTransition(10.s) {
            onFinished {
                business.refresh()  //<2>
            }
        }
    }.playFromStart()
    //end::bucle[]

    business.refresh()
}

