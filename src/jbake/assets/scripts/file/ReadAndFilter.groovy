
if( args.length != 3){
    println "Hey necesito el fichero de entrada, el de salida y la cadena a buscar"
    return
}

inputFile = new File(args[0])
outputFile = new File(args[1])
search = args[2]

outputFile.newWriter().withWriter { writer-> // <1>
    inputFile.eachLine { line, indx -> // <2>
        if (line.indexOf(search) != -1)
            writer << "$indx: $line\n" // <3>
    }
}
