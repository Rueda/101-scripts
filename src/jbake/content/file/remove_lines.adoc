= Eliminar lineas con error
Jorge Aguilera <jorge.aguilera@puravida-software.com>
2018-02-01
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc,files, removeIf
:jbake-category: file
:jbake-script: /scripts/file/RemoveLines.groovy
:idprefix:
:imagesdir: ../images



A continuación vamos a ver un script realmente sencillo para un caso de uso frecuente:

Tenemos un fichero que contiene un elevado número de líneas y en otro fichero tenemos identificadas las lineas que
queremos suprimir del primero (tal vez sean unos IDs, nombres, etc).

La idea es que tendríamos que recorrer el fichero
que contiene los registros a suprimir y buscar en el fichero de origen aquellos que cumplan la codición reescribiendo
una y otra vez el fichero hasta haber procesado todos los registros del fichero de error.

Mediante este script lo que hacemos es leer el fichero de origen y convertirlo a una lista (obviamente este script está
diseñado para ficheros con varios miles de lineas pero con un tamaño que no exceda el de la memoria disponible). Después
iremos eliminando de esta lista aquellos registros que cumplan la condición específica, para lo que usaremos el método
`removeIf` de Groovy

[source,groovy]
----
include::{sourcedir}{jbake-script}[]
----
<1> Suponemos que el primer campo de cada linea es el Id a buscar
<2> Eliminamos de la lista todos los registros que cumplan la condicion de comenzar por el ID, por ejemplo.
<3> Generamos un fichero nuevo uniendo la lista con retornos de carro




