= Tostando imágenes
Jorge Aguilera <jorge.aguilera@puravida-software.com>
2017-10-14
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc
:jbake-category: docker
:jbake-script: /scripts/docker/WatchFile.groovy
:idprefix:
:imagesdir: ../images


En el post *Docker y Groovy (básico)* vimos cómo podemos hacer que la imagen oficial de Groovy para Docker ejecute nuestros
scripts en host donde no se encuentre instalado Groovy. Así mismo esta imagen nos permite aprovechar todas las
características de Docker como montar volúmenes, conexión entre contenedores, etc de tal forma que nuestros
scripts puedan interactuar con otros contenedores y/o sistema anfitrión.

En este post lo que vamos a ver es cómo utilizarla como base para crear nuestras propias imágenes de tal forma que
podamos subirlas a nuestro repositorio ( Hub Docker, nexus privado, Gitlab, Google o cualquier otro en el que tengas
cuenta).

Para este ejemplo vamos a utilizar un script muy sencillo cuya función va a ser mostrar por pantalla el contenido
de un fichero que le pasemos por parámetro y esperar 30seg para volver a repetir la acción (vamos loq ue viene siendo
a ser un bucle infinito de toda la vida).

.WatchFile.groovy
[source,groovy]
----
include::{sourcedir}{jbake-script}[]
----

INFO:: Como puedes ver el script no es lo más importante en este artículo

== Dockerfile

Lo primero que vamos a necesitar es un fichero de instrucciones para que Docker pueda crear nuestra imagen.
Este fichero suele tener el nombre de Dockerfile y para nuestro ejemplo tiene esta pinta:

[source]
----
FROM groovy:2.4.12-jre8-alpine

COPY WatchFile.groovy /home/groovy/

VOLUME ["/var/watchfile"]

ENTRYPOINT ["groovy", "WatchFile.groovy"]

CMD []
----

Mediante este fichero simplemente indicamos de qué imagen vamos a extender (*groovy:2.4.12-jre8-alpine*), copiamos
nuestro script en la imagen e indicamos qué se debe ejecutar al arrancar ( ``groovy WatchFile.groovy``).

También preparamos nuestra imagen para que si no le proporcionamos parámetros de ejecución el script lo detecte
y nos pueda mostrar alguna ayuda de cómo ejecutarlo, o realizar una acción por defecto, (*CMD []*)

Así mismo vamos a montar un volumen particular para esta imagen en */var/watchfile*, lo cual nos permitirá, por
ejemplo, montar volúmenes de otros contenedores o del host anfitrión para que nuestro script pueda "vigilarlo"

== Generando la imagen

Desde el directorio donde tengamos estos dos ficheros ejecutaremos:

[source,console]
----
docker build --rm -t jagedn/watchfile .  //<1>
----
<1> jagedn/watchfile será el nombre de nuestra imagen. Fijate que el comando acaba en un punto

Si todo va bien, en tu máquina existirá ahora una imagen *jagedn/watchfile* (o el nombre que le hayas dado)

Para comprobarlo puedes ejecutar

----
docker images
----

== Ejecutando la imagen

Para ejecutar la imagen simplemente haremos:

[source,console]
----
docker run --rm -it -v /un/path/cualquiera:/var/watchfile jagedn/watchfile /var/watchfile/mifichero.log //<1>
----

Esto debe ejecutar un proceso que cada 30 segundos nos muestre el contenido de *mifichero.log*


Como puedes ver, este ejemplo en sí no realiza nada interesante salvo que muestra los pasos a realizar para
hacer que nuestro script se pueda distribuir y ejecutar en un entorno dockerizado por lo que es perfectamente
integrable en pipelines de continuous delivery por ejemplo.

Recuerda que para poder subir tu imagen a un repositorio simplemente deberás hacer un *push*, por ejemplo:

----
docker push jagedn/watchfile
----

