= Ejecutar comandos
Miguel Rueda <miguel.rueda.garcia@gmail.com>
2017-08-31
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc
:jbake-category: basico
:jbake-script: /scripts/basico/CommandLocal.groovy
:idprefix:
:imagesdir: ../images


Es muy común en el día a día que tengamos que ejecutar comandos de shell de forma repetida y en función de la respuesta
del mismo optar por realizar acciones, como por ejemplo ejecutar un comando u otro, etc. Estos comandos van desde un
simple listado de ficheros ( `dir` ), copiar ficheros ( `cp`, `copy` ) a otros más elaborados.

Así mismo numerosas veces debemos ejecutar esos comandos de forma repetida (una vez por cada directorio, por cada fichero,
etc) e incluso condicional (si la invocación de este comando ha ido bien realizar estas acciones y si no estas otras).
Para ello solemos recurrir a ficheros por lotes ( .bat en Windows, .sh en Linux ) donde podemos tratar los problemas
comentados anteriormente.

En este entorno, Groovy nos ofrece poder ejecutar comandos con la ayuda del método `.execute()` de la clase String
y tratar la salida de este como si fuera una cadena, utilizando toda la potencia del lenguaje.

== Comando simple

Supongamos que en un sistema *NIX quisieramos realizar un listado de los ficheros de un directorio y mostrar la salida
en mayúsculas. Nuestro script sería:

----
String resultado = "ls -lt ".execute().text
println resultado.toUpperCase()
----

Como podemos observar, simplemente tenemos que construir un String con el comando y llamar a su método `execute()`
Este método nos devuelve un objeto que entro otras cosas nos ofrece la salida del comando como una cadena mediante la
property *text* que podemos asignar a una variable

== Esperar finalización

Si lo que necesitamos es lanzar un comando y esperar a que termine para lanzar de nuevo otro comando u otra acción
se puede realizar de la siguiente manera:

[source,groovy]
----
include::{sourcedir}{jbake-script}[lines=1..14]
----
<1> Definimos la variables donde volcaremos el resultado de nuestro `execute`.
<2> Ejecutamos el comando.
<3> Obtenemos la salida del comando resultado de su ejecución.
<4> Establecemos un _time out_ a nuestro comando.
<5> En `error` obtendremos la salida en caso de que falle nuestro comando y en caso de funcionar correctamente nuestro
valor se guardará en resultado.
<6> El resultado es correcto podemos continuar.


